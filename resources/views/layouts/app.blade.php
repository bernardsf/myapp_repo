<!DOCTYPE html>
<html lang="{{ config('app.Locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name','myapp')}}</title>

        
    </head>
    <body >
          <div class="container">
          @yield('content')
          </div>

    </body>
</html>
