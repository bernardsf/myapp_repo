<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Pagescontroller extends Controller
{   
    public function index(){
        $title = 'Welcome To Laravel!';
       // return view('pages.index', compact ('title') );  // in this public function the controller did not working what want to show
        return view('pages.index', compact ('title') ) ->with('title',$title);
    }

    public function about(){
        $title = 'About Us';
        return view('pages.about') ->with('title',$title);
    }
    public function services(){
        $data = array (
            'title' => 'services',
            'services' => ['Web Design', 'Programming', 'SEO' ]
        );
        return view('pages.services')->with($data);
    }

        
}
